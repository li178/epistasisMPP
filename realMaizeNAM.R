# Real example (maizeNAM design) for analysis in:
# "A One-dimension Genome Scan Using the Mixed Model Approach Reveals QTL-by-genetic-background Interaction in Diallel and Nested Association Mapping designs"
library(statgenMPP)
library(asreml)
library(asremlPlus)
library(psych)
library(dplyr)
library(tidyr)
library(ggplot2)
source('R/functions_asreml4.r')
MPPdesign <- 'maizeNAM'
poptype <- 'DH'
dir <- paste0('data/',MPPdesign)

# IBD calculation and input data prepare----

phenoDat <- read.delim(paste0(dir,'/Inputs/',MPPdesign,'_pheno.txt'))
colnames(phenoDat)[1:2] <- c('genotype', 'cross')
crosses <- unique(phenoDat$cross)
crossfiles<-paste0(dir,'/Inputs/',crosses,'.txt')
phenoDat<-phenoDat[,-which(colnames(phenoDat)=='cross')]

MPPobj<- calcIBDMPP(crossNames = crosses,
                    markerFiles = crossfiles,
                    pheno = phenoDat,
                    popType = poptype,
                    mapFile = paste0(dir,'/Inputs/map.txt'),
                    evalDist = 1, # IBD calculated at grid of 1cM
                    verbose = TRUE)

plot(MPPobj,plotType = 'genMap')
plot(MPPobj,plotType = 'pedigree')
writeFiles(paste0(dir,'/Inputs/'),MPPobj,MPPdesign) 

# read data----
progPheno <- read.csv(paste0(dir,'/Inputs/',MPPdesign,'_phenotype.csv'))
progPheno$pop <- factor(progPheno$pop)
parGeno <- read.csv(paste0(dir,'/Inputs/',MPPdesign,'_parGeno.csv'),stringsAsFactors = F)
progHap <- read.csv(paste0(dir,'/Inputs/',MPPdesign,'_progIBD.csv'),stringsAsFactors = F)
map <- read.csv(paste0(dir,'/Inputs/',MPPdesign,'_map.csv'),stringsAsFactors = F)

#analysis----
threshold <- -log10(0.05/nrow(map)) # Bonferroni-adjusted threshold

str.residual <- 'hete' #'hete' --> family-specific（heterogeneous）residual structure 
#'homo' --> homogeneous residual structure

MQM <- TRUE # TRUE --> multi-QTL model with cofactors (multiple rounds of genome scans)
# FALSE --> single QTL model (one round of genome scan)
maxCofactors <- 10 # the max number of QTL candidates 

QTLwindow <- 20 # QTL window size (only works when setting MQM=TRUE )

allMapRes <- list()
for (trait.name in c("DMY","PH","DtSILK")) {
  ## general IBD-based QTL mapping (rP model)
  allMapRes[[trait.name]]$rPmod <- selQTL.IBD(parGeno,
                                              progHap,
                                              progPheno,
                                              map,
                                              trait.name,
                                              QTLwindow,
                                              threshold,
                                              str.residual,
                                              MQM,
                                              maxCofactors)
  
  ## 1-D IBD-based QTL mapping (cP&F model)
  allMapRes[[trait.name]]$cPFmod <- selEpisQTL.IBD(parGeno,
                                                   progHap,
                                                   progPheno,
                                                   map,
                                                   trait.name,
                                                   QTLwindow,
                                                   threshold,
                                                   str.residual,
                                                   MQM,
                                                   AIC.threshold = 0, # by default 0 --> select the model with the lower AIC
                                                   maxCofactors)
  ## Plot
  plotcPFvsrP(allMapRes,trait.name)  
}

#summary----
(alltraits <- names(allMapRes))
Rsq.rP2cPF.ls<-list()
QTL.rP2cPF.ls<-list()
diffResidual.rP2cPF.ls <- list()
for (t in 1:length(alltraits)) { 
  
  (trait.name<-alltraits[t])
  print (trait.name)
  ## Plot
  tiff(paste0(dir,'/results/',MPPdesign,'_',trait.name,'.tiff'), width = 600, height = 300)
  plotcPFvsrP(allMapRes,trait.name)
  dev.off()
  
  ## calc R^2
  candQTL_rPmod<-list(cofactors=allMapRes[[trait.name]]$rPmod[[1]],
                      definCof=rep('parent',length(allMapRes[[trait.name]]$rPmod[[1]])))
  
  candQTL_cPFmod<-allMapRes[[trait.name]]$cPFmod[[1]]
  
  rP2cPF.Table<-QTLrP2cPF(candQTL_rPmod,candQTL_cPFmod,QTLwindow)
  
  QTLrP<-list(cofactors=rP2cPF.Table$QTL[rP2cPF.Table$rp],definCof=rep('parent',sum(rP2cPF.Table$rp)))
  QTLcPF<-list(cofactors=rP2cPF.Table$QTL[rP2cPF.Table$cPF],definCof=rP2cPF.Table$defin[rP2cPF.Table$cPF])
  
  
  rP.Rsq<-round(calcR_sq_all(candQTL_rPmod,trait.name,parGeno,progPheno,progHap,map),2)
  rP.diffResidual <- diff_residual(candQTL_rPmod,trait.name,parGeno,progPheno,progHap,map)
  rP.diffResidual$model <- 'rP'
  
  cPF.Rsq<-round(calcR_sq_all(candQTL_cPFmod,trait.name,parGeno,progPheno,progHap,map),2)
  cPF.diffResidual <- diff_residual(candQTL_cPFmod,trait.name,parGeno,progPheno,progHap,map)
  cPF.diffResidual$model <- 'cPF'
  # sharing and unique QTLs in rP vs. cPF
  rP2cPF.Table$trait<-trait.name
  rP2cPF.Table$MPPdesign<-MPPdesign
  rP2cPF.Table<-cbind(rP2cPF.Table[,match(c('MPPdesign','trait'),names(rP2cPF.Table))],
                      rP2cPF.Table[,-match(c('MPPdesign','trait'),names(rP2cPF.Table))])
  
  onlyrP<-rP2cPF.Table$rp==T&rP2cPF.Table$cPF==F
  onlycPF<-rP2cPF.Table$rp==F&rP2cPF.Table$cPF==T
  bothrPcPF<-rP2cPF.Table$rp==T&rP2cPF.Table$cPF==T
  (rP2cPF.Table<-rP2cPF.Table %>% mutate(onlyrP=onlyrP,onlycPF=onlycPF,bothrPcPF))
  QTL.rP2cPF.ls[[paste0(MPPdesign,'_',trait.name)]]<-rP2cPF.Table
  
  Rsq.rP2cPF.ls[[paste0(MPPdesign,'_',trait.name)]]<-data.frame(MPPdesign=MPPdesign,trait=trait.name,
                                                                rP.Rsq=rP.Rsq,cPF.Rsq=cPF.Rsq)
  
  diffResidual <- rbind(rP.diffResidual, cPF.diffResidual)
  diffResidual.rP2cPF.ls[[paste0(MPPdesign,'_',trait.name)]] <- diffResidual %>% mutate(MPPdesign=MPPdesign,trait=trait.name)
}

(QTL.rP2cPFtb<-do.call('rbind',QTL.rP2cPF.ls))
(Rsq.rP2cPFtb<-do.call('rbind',Rsq.rP2cPF.ls))
(diffResidual.rP2cPFtb<-do.call('rbind',diffResidual.rP2cPF.ls))
diffResidual.rP2cPFtb$model <- factor(diffResidual.rP2cPFtb$model,levels = c('rP','cPF'))
meanValues <- as.data.frame(diffResidual.rP2cPFtb %>% 
                              # Group the data by brand then get means
                              group_by(model, trait) %>% 
                              summarise(mean_residualVar.diff= mean(residualVar.diff )))

plot.diffResidual <-
  ggplot(data = diffResidual.rP2cPFtb,
         aes(x=model,y=residualVar.diff,group=family,color=family)) +
  geom_line()+
  geom_point(size=2) +
  xlab('Model')+
  ylab('Change of residual variance in each family \n (null model - final QTL model)')+
  facet_grid(trait~.,scales="free_y")+
  
  theme(
    plot.title = element_text(hjust = 0.5),
    panel.background = element_blank(),
    plot.background = element_blank(),
    axis.ticks.x = element_blank(),
    legend.position = "top",
    strip.background = element_blank(),
    panel.border = element_rect(fill = NA, color = "black",size = 0.5, linetype = "solid"))

tiff(paste0(dir,'/results/',MPPdesign,'_diffResidual.tiff'), width = 800, height = 800)
print(plot.diffResidual)
dev.off()

QTLclass <- list()
for (trait.name in alltraits) {
  df <- QTL.rP2cPFtb %>% filter(trait ==trait.name) 
  
  total <- nrow(df)
  onlyrP <- sum(df$onlyrP)
  
  family.both <- sum(df$defin=='family'& df$bothrPcPF==TRUE)
  parent.both <- sum(df$defin=='parent'& df$bothrPcPF==TRUE)
  
  family.onlycPF <- sum(df$defin=='family'& df$onlycPF==TRUE)
  parent.onlycPF <- sum(df$defin=='parent'& df$onlycPF==TRUE)
  
  QTLclass[[trait.name]] <- c(total, onlyrP, family.both, parent.both, family.onlycPF, parent.onlycPF)
  names(QTLclass[[trait.name]]) <- c('total', 'onlyrP','family.both', 'parent.both', 'family.onlycPF', 'parent.onlycPF')
}
QTLclass

# save----
save(list = ls(),file=paste0(dir,'/Results/',MPPdesign,'.rdata'))




